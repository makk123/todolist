import React, {FC,ChangeEvent,useState}from 'react';
import './App.css';
import {ITask} from "./components/interfaces";
import {IITasks} from "./components/todoTask"
const App: FC = () => {
  const[task , setTask] = useState<string>("");
  const[todoList , setTodoList] = useState<ITask[]>([]);

  const handleChange = (event: ChangeEvent<HTMLInputElement>):void=>{

     setTask(event.target.value);

  };

  const addTask = ():void => {

 const newTask = { taskName : task};
  setTodoList([...todoList,newTask]);
  console.log(todoList);

  }

  return (
    <div className="App">
       <div className="header">
         <input type ="text" placeholder ="..Task" onChange={handleChange}></input>
         <button onClick ={ addTask }>Add Task</button>

       </div>

       <div className='TodoList'>

       </div>
   
   
   
   <>
   </>
    </div>
  );
}

export default App;
